package br.ucsal.bes.poo20191.atividade04.lucassamir;

public class TestAgenda {

	public static void main(String[] args) {
		Agenda agenda = new Agenda();

		agenda.incluir("Dayana", "(87) 8129-991", 1995, "05/06/1995", enumeracao.PESSOAL);

		agenda.incluir("Gleydson", "(87) 8159-1822", 1996, "22/02/1996", enumeracao.PESSOAL);

		agenda.incluir("Isabelle", "(11) 96119-7976", 1996, "22/03/1996", enumeracao.PROFISSIONAL);

		agenda.incluir("Emily", "(71) 9185-8014", 1996, "02/08/1996", enumeracao.PROFISSIONAL);

		agenda.incluir("Carlos", "(97) 98108-0851", 1997, "15/10/1997", enumeracao.PROFISSIONAL);

		agenda.listar();

		agenda.pesquisarNome("Isabelle");

		agenda.pesquisarNome("Emily");

		agenda.excluir("Isabelle");

		agenda.excluir("Emily");

		agenda.listar();
	}
}

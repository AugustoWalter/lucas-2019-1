package br.ucsal.bes.poo20191.atividade04.lucassamir;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Contato {

	private String nome;
	private String telefone;
	private Integer anoDeNascimento;
	private Date dataDeNascimento;
	private enumeracao tipo;
	private SimpleDateFormat formatdata = new SimpleDateFormat("dd/MM/yyyy");

	public Contato(String nome, String telefone, Integer anoDeNascimento, String dataDeNascimento, enumeracao tipo) {
		this.nome = nome;
		this.telefone = telefone;
		this.anoDeNascimento = anoDeNascimento;
		try {
			this.dataDeNascimento = formatdata.parse(dataDeNascimento);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public enumeracao getTipo() {
		return tipo;
	}

	public void setTipo(enumeracao tipo) {
		this.tipo = tipo;
	}

	public String toString() {
		return String.format("Nome: " + nome + "; Telefone: " + telefone + "; Ano De Nascimento: " + anoDeNascimento
				+ "; Data De Nascimento: " + formatdata.format(dataDeNascimento) + "; Tipo :" + tipo);
	}

}

package br.ucsal.bes.poo20191.atividade04.lucassamir;

import java.util.ArrayList;

public class Agenda {

	private ArrayList<Contato> contatos = new ArrayList<>();

	public void incluir(String nome, String telefone, Integer anoDeNascimento, String dataDeNascimento,
			enumeracao tipo) {
		contatos.add(new Contato(nome, telefone, anoDeNascimento, dataDeNascimento, tipo));
	}

	public void excluir(String nome) {
		System.out.println("\nProcesando exclus�o: " + nome);
		Contato contato = pesquisarNome(nome);

		if (contato != null) {
			contatos.remove(contato);
			System.out.println("\nExclus�o confirmada.");
		}
	}

	public void listar() {

		System.out.println("\nListar Nomes:");
		for (Contato contato : contatos) {
			System.out.println(contato);
		}
	}

	public Contato pesquisarNome(String nome) {
		for (Contato dado : contatos) {
			if (dado.getNome().equals(nome)) {
				System.out.println("\nContato encontrado: \n" + dado);
				return dado;
			}
		}
		return null;
	}
}
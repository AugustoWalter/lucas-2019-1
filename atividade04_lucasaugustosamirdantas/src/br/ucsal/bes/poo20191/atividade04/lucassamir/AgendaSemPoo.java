package br.ucsal.bes.poo20191.atividade04.lucassamir;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class AgendaSemPoo {

	public static void main(String[] args) {
		String[] nome = new String[1000];
		String[] telefone = new String[1000];
		Integer[] anoDeNascimento = new Integer[1000];
		String[] dataDeNascimento = new String[1000];
		String[] enumeracaoPessoalProfissional = new String[1000];

		SimpleDateFormat formatdata = new SimpleDateFormat("dd/MM/yyyy");

		for (int i = 0; i < 100; i++) {
			nome[i] = "";
			telefone[i] = "";
			anoDeNascimento[i] = 0;
			dataDeNascimento[i] = "";
			enumeracaoPessoalProfissional[i] = "";

		}

		int opcao = 0;
		int posicao = 0;
		String continuar = "";
		String nomeExcluir = "";
		String nomePesquisa = "";
		Scanner scanner = new Scanner(System.in);

		do {

			System.out.println("Escolha a op��o: 1-Incluir  2-Listar  3-Excluir  4-Sair");
			opcao = scanner.nextInt();
			scanner.nextLine();

			switch (opcao) {
			case 1:
				// C�digo para Incluir

				if (posicao < nome.length) {
					do {

						System.out.print("Digite o nome: ");
						nome[posicao] = scanner.nextLine();

						System.out.print("Digite o telefone: ");
						telefone[posicao] = scanner.nextLine();

						System.out.print("Digite o ano de nascimento: ");
						anoDeNascimento[posicao] = scanner.nextInt();
						scanner.nextLine();

						try {
							System.out.print("Digite a data de nascimento: ");
							dataDeNascimento[posicao] = scanner.nextLine();

							DateFormat df = new SimpleDateFormat("dd/MM");
							Date dt = df.parse(dataDeNascimento[posicao]);
							System.out.println(dt);
						} catch (Exception ex) {
							ex.printStackTrace();
						}

						System.out.print("Contato Pessoal ou Profissional: ");
						enumeracaoPessoalProfissional[posicao] = scanner.nextLine();

						System.out.print("Deseja continuar o cadastramento? 1-Sim  2-N�o ");
						continuar = scanner.nextLine();

						posicao++;

					} while (continuar.equals("1"));

				} else {

					System.out.println("Sua agenda est� cheia.");

				}

				break;

			case 2:

				for (int i = 0; i < 1000; i++) {
					if (!nome[i].equals("")) {

						System.out.println("Nome: " + nome[i] + " " + "Telefone: " + telefone[i] + " "
								+ "Ano de Nascimento: " + anoDeNascimento[i] + " " + "Data de Nascimento: "
								+ dataDeNascimento[i] + " " + "Enumera��o: " + enumeracaoPessoalProfissional[i]);
					}

				}

				break;

			case 3:
				// C�digo para Excluir
				System.out.println("Quem deseja excluir? ");
				nomeExcluir = scanner.nextLine();

				for (int i = 0; i < 100; i++) {
					if (nome[i].equals(nomeExcluir)) {

						nome[i] = "";
						telefone[i] = "";
						anoDeNascimento[i] = 0;
						dataDeNascimento[i] = "";
						enumeracaoPessoalProfissional[i] = "";

					}

				}

				break;
			case 4:
				// C�digo para Sair
				System.out.println("Programa Finalizado.");
				return;

			default:
				// Op��o Invalida!
				System.out.println("Op��o Inv�lida! Tente novamente.");
				break;
			}

		} while (opcao != 4);

	}
}
package fundamentos;

import javax.swing.JOptionPane;

public class ConversaoStringNumero {
   
	public static void main(String[] args) {
		String resposta1 = JOptionPane.showInputDialog("Qual � a nota 1?");
		String resposta2 = JOptionPane.showInputDialog("Qual � a nota 2?");
  	    
	    System.out.println(resposta1 + "\n" + resposta2); 
	
	    // Converter String em Doble
	    double nota1 = Double.parseDouble(resposta1);
	    double nota2 = Double.parseDouble(resposta2);
	    double total = nota1 + nota2;
	    
	    System.out.println(total / 2);
	
     	// Outras convers�es poss�veis
	    /*
	     * Inteiros
	     * Byte.parseByte("1");
	     * Short.parseShort("2");
	     * Integer.parseInt("3");
	     * Long.parseLong("4");
	     * 
	     * Reais (Ponto flutuante)
	     * Float.parseFloat("1.2");
	     * Double.parseDouble("2.4");
	     * 
	     * Booleano
	     * Boolean.parseBoolean("false");
	     */
	    
	
	}
	
}

package controle;

import java.util.Scanner;

public class Exercicio4 {

	/**
	 * 4. Criar um programa que receba um número e diga se ele é um número primo.
	 *
	 * private static boolean isPrimo(int numero) { for (int j = 2; j < numero; j++)
	 * { if (numero % j == 0) return false; } return true; }
	 */

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("\nDigite um número para verificar se é primo");
		int numero = scanner.nextInt();
		int contadorDeDivisores = 0;

		for (int i = 2; i < numero; i++) {
			if (numero % i == 0) {
				contadorDeDivisores++;

			}
		}
		if (contadorDeDivisores == 0) {
			System.out.println("\nO número " + numero + " é primo.");

		} else {
			System.out.println("\nO número " + numero + " não é primo");
		}

	}
}

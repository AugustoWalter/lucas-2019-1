package colecoes;

import java.util.Stack;

public class Pilha {

	public static void main(String[] args) {
		// posso usar os fundamentos da fila e pilha juntos usando o Deque e o
		// ArrayDeque
		Stack<String> pilha = new Stack<>();

		pilha.push("O Pequeno Pr�ncipe");
		pilha.push("O Hobbit");
		pilha.push("Don Quixote");

		System.out.println("peek");
		System.out.println(pilha.peek());

		System.out.println("pop...");
		System.out.println(pilha.pop());
		System.out.println(pilha.pop());
		System.out.println(pilha.pop());
		// System.out.println(pilha.pop());
	}
}

package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

/*
 * 1. Crie um programa em Java que:
a) Leia o nome;
b) Leia o sobrenome;
c) Concatene o nome com o sobrenome;
d) Apresente o nome completo.
 */

public class NomeSobrenome {

	public static void main(String[] args) {
		// Scanner scanner = new Scanner(System.in);
		// Declara��o de vari�veis
		String nome;
		String sobrenome;
		String nomeCompleto;

		// Entrada de dados
		// System.out.println("Informe o nome:");
		nome = obterString("Informe o nome:");
		// System.out.println("Informe o sobrenome:");
		sobrenome = obterString("Informe o sobrenome:");

		// Processamento
		nomeCompleto = calcularNomeCompleto(nome, sobrenome);

		exibirNomeCompleto(nomeCompleto);

	}

	private static String obterString(String mensagem) {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return scanner.nextLine();
	}

	private static String calcularNomeCompleto(String nome, String sobrenome) {
		String nomeCompleto;
		nomeCompleto = nome + " " + sobrenome;
		return nomeCompleto;
	}

	private static void exibirNomeCompleto(String nomeCompleto) {
		System.out.println("Nome completo: " + nomeCompleto.toUpperCase());
	}

}
/*
 * // Declarao de variveis
 * 
 * @SuppressWarnings("resource") Scanner sc = new Scanner(System.in); String
 * nome; String sobrenome; String nomeCompleto;
 * 
 * // Entrada de dados System.out.println("Informe o nome:"); nome =
 * sc.nextLine(); System.out.println("Informe o sobrenome:"); sobrenome =
 * sc.nextLine();
 * 
 * // Processamento nomeCompleto = nome + " " + sobrenome;
 * 
 * // Sada System.out.println("Nome completo = " + nomeCompleto); }
 * 
 * }
 * 
 */

package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

/*
 * Crie um programa em Java para ler uma letra do alfabeto e mostrar uma mensagem: se �
vogal ou consoante.
 */

public class Vogal_Ou_Consoante {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite uma letra do alfabeto PT BR: ");
		String letra = scanner.next();

		if ((letra.equalsIgnoreCase("A")) || (letra.equalsIgnoreCase("E")) || (letra.equalsIgnoreCase("I"))
				|| (letra.equalsIgnoreCase("O")) || (letra.equalsIgnoreCase("U"))) {
			System.out.println("A letra digitada � uma Vogal: " + letra);

		} else {
			System.out.println("A letra digita � uma Cosoante: " + letra);
		}
		scanner.close();
	}
}
package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

/*
 * a) Leia uma sequ�ncia de 5 n�meros inteiros. No processo de obten��o dos n�meros, seu
programa dever� exibir a mensagem: �Informe 5 n�meros:� e ap�s esta mensagem
permitir a entrada dos valores;
b) Ap�s a entrada dos n�meros, verifique qual o maior dentre os n�meros informados;
c) Apresente como resposta o maior dentre os n�meros informados: �Dentre os n�meros
informados, o maior foi <maior>�.
d) Na sua solu��o crie os seguintes m�todos:
1. void obterNumeros(int[] vet);
2. int encontrarMaiorNumero (int[] vet);
3. void exibirMaiorNumero(int maior);
 */

public class maiorValor {

	public static void main(String[] args) {


		int[] vet = new int[5];
		int maiorNumero = 0;
		obterNumeros(vet);
		maiorNumero = encontrarMaiorNumero(vet, maiorNumero);
		exibirMaiorNumero(maiorNumero);
	}


	public static void obterNumeros(int[] vet) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe " + vet.length + " numeros: ");
		for(int i = 0; i < vet.length; i++) {
			vet[i] = scanner.nextInt();
		}
	}	

	public static int encontrarMaiorNumero(int[] vet, int maiorNumero) {

		if (vet[0] > vet[1] && vet[0] > vet[2] && vet[0] > vet[3] && vet[0] > vet[4]) {
			maiorNumero = vet[0];

		} else if (vet[1] > vet[0] && vet[1] > vet[2] && vet[1] > vet[3] && vet[1] > vet[4]) {
			maiorNumero = vet[1];

		} else if (vet[2] > vet[0] && vet[2] > vet[1] && vet[2] > vet[3] && vet[2] > vet[4]) {
			maiorNumero = vet[2];

		} else if (vet[3] > vet[0] && vet[3] > vet[1] && vet[3] > vet[2] && vet[3] > vet[4]) {
			maiorNumero = vet[3];

		} else if (vet[4] > vet[0] && vet[4] > vet[1] && vet[4] > vet[2] && vet[4] > vet[3]) {
			maiorNumero = vet[4];
			return maiorNumero;
		}
		return maiorNumero;
	}
	private static void exibirMaiorNumero(int maiorNumero) {

		System.out.println("O maior n�mero informado �: " + maiorNumero);

	}

}
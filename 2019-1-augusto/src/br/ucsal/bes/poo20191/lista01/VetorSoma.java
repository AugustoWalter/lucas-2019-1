package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

public class VetorSoma {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int vetor1[] = new int[10];

		int vetor2[] = new int[10];

		int vetor3[] = new int[10];

		System.out.println("Digite 10 n�meros inteiros.");
		int num1 = scanner.nextInt();
		int num2 = scanner.nextInt();
		int num3 = scanner.nextInt();
		int num4 = scanner.nextInt();
		int num5 = scanner.nextInt();
		int num6 = scanner.nextInt();
		int num7 = scanner.nextInt();
		int num8 = scanner.nextInt();
		int num9 = scanner.nextInt();
		int num10 = scanner.nextInt();

		vetor1[0] = num1;
		vetor1[1] = num2;
		vetor1[2] = num3;
		vetor1[3] = num4;
		vetor1[4] = num5;
		vetor1[5] = num6;
		vetor1[6] = num7;
		vetor1[7] = num8;
		vetor1[8] = num9;
		vetor1[9] = num10;

		System.out.println("Processando o vetor2...\n");
		System.out.println("Digite 10 n�meros inteiros.");
		int valor1 = scanner.nextInt();
		int valor2 = scanner.nextInt();
		int valor3 = scanner.nextInt();
		int valor4 = scanner.nextInt();
		int valor5 = scanner.nextInt();
		int valor6 = scanner.nextInt();
		int valor7 = scanner.nextInt();
		int valor8 = scanner.nextInt();
		int valor9 = scanner.nextInt();
		int valor10 = scanner.nextInt();

		vetor2[0] = valor1;
		vetor2[1] = valor2;
		vetor2[2] = valor3;
		vetor2[3] = valor4;
		vetor2[4] = valor5;
		vetor2[5] = valor6;
		vetor2[6] = valor7;
		vetor2[7] = valor8;
		vetor2[8] = valor9;
		vetor2[9] = valor10;

		vetor3[0] = vetor1[0] + vetor2[0];
		vetor3[1] = vetor1[1] + vetor2[1];
		vetor3[2] = vetor1[2] + vetor2[2];
		vetor3[3] = vetor1[3] + vetor2[3];
		vetor3[4] = vetor1[4] + vetor2[4];
		vetor3[5] = vetor1[5] + vetor2[5];
		vetor3[6] = vetor1[6] + vetor2[6];
		vetor3[7] = vetor1[7] + vetor2[7];
		vetor3[8] = vetor1[8] + vetor2[8];
		vetor3[9] = vetor1[9] + vetor2[9];

		System.out.println("As somas dos vetores 1 e 2 s�o: " + vetor3[0] + " " + vetor3[1] + " " + vetor3[2] + " "
				+ vetor3[3] + " " + vetor3[4] + " " + vetor3[5] + " " + vetor3[6] + " " + vetor3[7] + " " + vetor3[8]
				+ " " + vetor3[9]);

		scanner.close();
	}

}

package br.ucsal.bes.poo20191.lista01.testes;

import org.junit.jupiter.api.Test;

import br.ucsal.bes.poo20191.lista01.maiorValor;
import junit.framework.Assert;

public class maiorValorTeste {
	@Test
	public void testMaiorValor() {
		int vetor[] = new int[5];

		vetor[0] = 4;
		vetor[1] = 8;
		vetor[2] = 7;
		vetor[3] = 4;
		vetor[4] = 3;

		Integer valorEsperado = vetor[1];

		int maiorNumero = 0;
		Integer valorAtual = maiorValor.encontrarMaiorNumero(vetor, maiorNumero);

		Assert.assertEquals(valorEsperado, valorAtual);
		System.out.println(valorAtual);
	}
}

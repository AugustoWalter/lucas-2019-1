package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

/*
 * 3. Suponha que o conceito de um aluno seja determinado em fun��o da sua nota. Suponha,
   tamb�m, que esta nota seja um valor inteiro na faixa de 0 a 100, conforme a seguinte faixa:
 Nota Conceito
  0 a 49 Insuficiente
  50 a 64 Regular
  65 a 84 Bom
  85 a 100 �timo
  /* 
   * Crie um programa em Java que leia a nota de um aluno e apresente o conceito do mesmo.
   */

public class ConceitoAlunoSwitch {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Digite a nota do Aluno: ");
		int nota = scanner.nextInt();

		String conceito = " ";

		switch (nota) {

		case 0:
		case 49:
			conceito = "Insuficiente";

			break;
		case 50:
		case 64:
			conceito = "Regular";
			break;

		case 65:
		case 84:
			conceito = "Bom";
			break;
		case 85:
		case 100:
			conceito = "�timo";
			break;
		default:
			System.out.println(conceito = null);
			break;

		}
		if (conceito != null) {
			System.out.println("O Aluno est�: " + conceito);
		} else {
			System.out.println("Nota inv�lida!");
		}
		scanner.close();
	}
}

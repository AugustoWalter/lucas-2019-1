package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

//Crie um programa em Java que permita a entrada de 10 valores em cada um dos dois
//vetores (vet1 e vet2). A seguir o programa dever armazenar num terceiro vetor (vet3) a soma
//do contedo dos dois vetores (vet1 e vet2). Por fim, o programa dever exibir os valores
//armazenados em vet3.

public class VetorSomaPoo {

	private static final int QTD_NUMEROS = 10;

	public static void main(String[] args) {
		// Declara��o de vari�veis
		Integer vet1[] = new Integer[QTD_NUMEROS];
		Integer vet2[] = new Integer[QTD_NUMEROS];
		Integer vet3[] = new Integer[QTD_NUMEROS];

		// Entrada de dados
		obterNumeros("vetor 1", vet1);
		obterNumeros("vetor 2", vet2);

		// Processamento
		somarVetores(vet3, vet1, vet2);

		// Sa�da
		exibirVetor("A soma dos vetores 1 e 2: ", vet3);

	}

	private static void obterNumeros(String nomeVetor, Integer[] vetor) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe " + vetor.length + " numeros para " + nomeVetor + ":");
		for (Integer i = 0; i < vetor.length; i++) {
			vetor[i] = scanner.nextInt();

		}

	}

	private static void somarVetores(Integer[] vet3Resultado, Integer[] vet1, Integer[] vet2) {
		for (Integer i = 0; i < vet3Resultado.length; i++) {
			vet3Resultado[i] = vet1[i] + vet2[i];
		}
	}

	private static void exibirVetor(String mensagem, Integer[] vet3Resultado) {
		System.out.println(mensagem);
		for (Integer i = 0; i < vet3Resultado.length; i++) {
			System.out.println(vet3Resultado[i]);

		}
	}
}

package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

/*
 * 2. Crie um programa em Java que:
a) Leia um n�mero inteiro;
b) Leia um segundo n�mero inteiro;
c) Efetue a adi��o dos dois valores;
d) Apresente o valor calculado.
 */
public class Adicao {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Digite o primeiro n�mero: ");
		int num1 = scanner.nextInt();

		System.out.println("Digite o segundo n�mero: ");
		int num2 = scanner.nextInt();

		int total = num1 + num2;
		System.out.println("O valor da soma �: " + total);

		scanner.close();
	}

}

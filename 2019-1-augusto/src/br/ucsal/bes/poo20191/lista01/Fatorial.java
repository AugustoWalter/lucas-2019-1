package br.ucsal.bes.poo20191.lista01;

//n! = n . (n � 1) . (n � 2) . (n � 3)!
/*
 * Fatorial de 2: 2! (l�-se 2 fatorial)
 * 
 * 2! = 2 . 1 = 2
 * 
 * Fatorial de 3: 3! (l�-se 3 fatorial)
 * 
 * 3! = 3 . 2 . 1 = 6
 * 
 * Fatorial de 4: 4! (l�-se 4 fatorial)
 * 
 * 4! = 4. 3 . 2 . 1 = 24
 */
import java.util.Scanner;

/*
 * 5. Crie um programa em Java que l� um valor N, inteiro e positivo, calcula e escreve o valor de
E (soma dos inversos dos fatoriais de 0 a N):
 */
public class Fatorial {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double fat = 1;

		System.out.println("Digite um n�mero inteiro e positivo: ");
		int num = scanner.nextInt();

		for (int i = 1; i <= num; i++) {
			fat *= i; // fat = fat * i;

		}
		System.out.println("O n�mero digatado foi: " + num + " e seu fatorial �" + " = " + fat);

		scanner.close();
	}
}
package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 9. Fa�a um algoritmo que leia uma temperatura em graus Celsius e apresente-a
convertida em graus Fahrenheit. A f�rmula de convers�o �: F = (9 * C + 160) / 5, na qual F
� a temperatura em Fahrenheit e C � a temperatura em Celsius;
 */

public class CelsiusParaFahrenheit {

	public static void main(String[] args) {
		float celsius = 0;
		float conversaoFahrenheit = 0;

		celsius = obterTemperaturaCelsius("Informe a Temperatura Celsius: ", celsius);

		conversaoFahrenheit = calcularConversao("Convers�o...", celsius, conversaoFahrenheit);

		exibirConversao("A temperatura em Fahrenheit �: ", conversaoFahrenheit, " F");

	}

	private static float obterTemperaturaCelsius(String mensagem, float celsius) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);

		return celsius = scanner.nextFloat();
	}

	private static float calcularConversao(String string, float celsius, float conversaoFahrenheit) {
		conversaoFahrenheit = (9 * celsius + 160) / 5;
		return conversaoFahrenheit;
	}

	private static void exibirConversao(String mensagem, float conversaoFahrenheit, String f) {
		System.out.println(mensagem + conversaoFahrenheit + f);
	}

}

package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 11. Faça um algoritmo que calcule e apresente o valor do volume de uma lata de óleo,
utilizando a fórmula VOLUME = π * RAIO2 * ALTURA. Utilizando π ( pi = 3,14159 ).
 */
public class VolumeLata {

	public static void main(String[] args) {
		double volume = 0;
		float raio = 0;
		float altura = 0;
		final float PI = 3.14159f;

		raio = obterRaio("Informe o raio da lata de óleo: ", raio);
		altura = obterAltura("Informe a altura da lata de óleo: ", altura);

		volume = calcularVolume(volume, raio, altura, PI);

		exibirVolume(volume);

	}

	private static float obterRaio(String mensagem, float raio) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return raio = scanner.nextFloat();
	}

	private static float obterAltura(String mensagem, float altura) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return altura = scanner.nextFloat();
	}

	private static double calcularVolume(double volume, float raio, float altura, float PI) {
		volume = PI * (Math.pow(raio, 2)) * altura;
		return volume;
	}

	private static void exibirVolume(double volume) {
		System.out.printf("O volume de uma lata de óleo é:%n%.2f ml ", volume);
	}
}

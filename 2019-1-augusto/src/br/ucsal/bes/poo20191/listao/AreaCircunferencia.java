package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 4. Faça um algoritmo para calcular a área de uma circunferência, considerando a fórmula
ÁREA = π * RAIO2. Utilize as variáveis AREA e RAIO, a constante π ( pi = 3,14159 ) e os
operadores aritméticos de multiplicação.
 */
public class AreaCircunferencia {

	public static void main(String[] args) {
		double area = 0;
		float raio = 0;
		final float PI = 3.14159f;

		raio = obterRaio("Digite o Raio: ", raio);

		area = calcularArea(area, raio, PI);

		exibirArea("A area da circunferência é: ", area);
	}

	private static float obterRaio(String mensagem, float raio) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return raio = scanner.nextFloat();
	}

	private static double calcularArea(double area, float raio, float PI) {
		area = PI * (raio * raio);
		// area = pi * (Math.pow(raio, 2));
		return area;
	}

	private static void exibirArea(String mensagem, double area) {
		System.out.println(mensagem + area);

	}
}
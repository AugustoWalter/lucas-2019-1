package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 18. Fa�a um algoritmo que leia dois valores inteiros (A e B) e apresente o resultado da
soma do quadrado de cada valor lido.
 */

public class SomaQuadradoDoisValores {

	public static void main(String[] args) {
		int valorA = 0;
		int valorB = 0;
		double quadradoA = 0;
		double quadradoB = 0;
		double somaTotalQuadrado = 0;

		valorA = obterValorA("Informe o valor A: ", valorA);
		valorB = obterValorB("Informe o valor B: ", valorB);

		quadradoA = calcularQuadradoA(quadradoA, valorA);
		quadradoB = calcularQuadradoB(quadradoB, valorB);

		somaTotalQuadrado = realizarSoma("Resultado de ValorA: ", "Resultado de ValorB: ", somaTotalQuadrado,
				quadradoA, quadradoB);

		exibirResultado("\nResultado da soma do quadrado de A e B: ", somaTotalQuadrado);

	}

	private static int obterValorA(String mensagem, int valorA) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);

		return valorA = scanner.nextInt();
	}

	private static int obterValorB(String mensagem, int valorB) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return valorB = scanner.nextInt();
	}

	private static double calcularQuadradoA(double quadradoA, int valorA) {
		quadradoA = valorA;
		quadradoA = Math.pow(quadradoA, 2);
		return quadradoA;
	}

	private static double calcularQuadradoB(double quadradoB, int valorB) {
		quadradoB = valorB;
		quadradoB = Math.pow(quadradoB, 2);
		return quadradoB;
	}

	private static double realizarSoma(String mensagem1, String mensagem2, double somaTotalQuadrado, double quadradoA,
			double quadradoB) {
		System.out.println(mensagem1 + quadradoA);
		System.out.println(mensagem2 + quadradoB);
		somaTotalQuadrado = quadradoA + quadradoB;
		return somaTotalQuadrado;
	}

	private static void exibirResultado(String mensagem, double somaTotalQuadrado) {
		System.out.println(mensagem + somaTotalQuadrado);

	}

}

package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 10. Fa�a um algoritmo que leia uma temperatura em Fahrenheit e a apresente convertida
em graus Celsius. A f�rmula de convers�o � C = (F � 32) * ( 5 / 9), na qual F � a
temperatura em Fahrenheit e C � a temperatura em Celsius.
 */

public class FahrenheitParaCelsius {

	public static void main(String[] args) {
		float fahrenheit = 0;
		float celsius = 0;

		fahrenheit = obterTemperaturaFahrenheit("Informe a temperatura em Fahrenheit: ", fahrenheit);

		celsius = calcularConversao(celsius, fahrenheit);

		exibirConversao("A temperatura em Celsius �: ", celsius, " C");

	}

	private static float obterTemperaturaFahrenheit(String mensagem, float fahrenheit) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return fahrenheit = scanner.nextFloat();
	}

	private static float calcularConversao(float celsius, float fahrenheit) {
		celsius = (fahrenheit - 32) * 5 / 9;
		return celsius;
	}

	private static void exibirConversao(String mensagem, float celsius, String c) {
		System.out.println(mensagem + celsius + c);

	}

}

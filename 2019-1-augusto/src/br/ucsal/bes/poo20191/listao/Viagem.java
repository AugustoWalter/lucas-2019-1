package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 *12. Fa�a um algoritmo que calcule a quantidade de litros de combust�vel gasta em uma
  viagem, por um autom�vel que faz 12Km por litro. O usu�rio deve fornecer o tempo gasto
  e velocidade m�dia da viagem. Calcule a dist�ncia percorrida com a f�rmula:
  DISTANCIA = TEMPO * VELOCIDADE.
  Tendo o valor da dist�ncia, calcule o consumo de combust�vel da viagem com a f�rmula:
  LITROS_USADOS = DISTANCIA / 12
  O programa deve apresentar os valores da velocidade m�dia, tempo gasto na viagem, a
  dist�ncia percorrida e a quantidade de litros utilizada na viagem.
 */

public class Viagem {

	public static void main(String[] args) {
		float tempoGasto = 0;
		float velocidadeMedia = 0;
		float distancia = 0;
		float litrosUsados = 12;

		tempoGasto = obterTempoGasto("Informe o tempo gasto na viagem: ", tempoGasto);
		velocidadeMedia = obterVelocidadeMedia("Informe a velocidade m�dia adquirida na viagem: ", velocidadeMedia);

		distancia = calcularDistanciaPercorrida(distancia, tempoGasto, velocidadeMedia);

		litrosUsados = calcularConsumoCombustivel(distancia, litrosUsados);

		exibirDadosViagem("Velocidade M�dia: ", velocidadeMedia, "Tempo gasto: ", tempoGasto, "Dist�ncia percorrida: ",
				distancia, "Quantidade de litros de gasolina: ", litrosUsados);

	}

	private static float obterTempoGasto(String mensagem, float tempoGasto) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return tempoGasto = scanner.nextFloat();
	}

	private static float obterVelocidadeMedia(String mensagem, float velocidadeMedia) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return velocidadeMedia = scanner.nextFloat();
	}

	private static float calcularDistanciaPercorrida(float distancia, float tempoGasto, float velocidadeMedia) {
		distancia = tempoGasto * velocidadeMedia;
		return distancia;
	}

	private static float calcularConsumoCombustivel(float distancia, float litrosUsados) {
		litrosUsados = distancia / litrosUsados;
		return litrosUsados;
	}

	private static void exibirDadosViagem(String string, float velocidadeMedia, String string2, float tempoGasto,
			String string3, float distancia, String string4, float litrosUsados) {
		System.out.printf(string + velocidadeMedia + "\n" + string2 + tempoGasto + "\n" + string3 + distancia + "\n"
				+ string4 + "%.2f L", litrosUsados);

	}
}

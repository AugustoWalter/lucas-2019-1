package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 13. Fa�a um algoritmo que leia dois valores para as vari�veis A e B e efetue a troca dos
valores de forma que a vari�vel A passe a possuir o valor da vari�vel B e a vari�vel B
passe a possuir o valor da vari�vel A. Ao final apresente os valores trocados.
 */

public class TrocaDeValores {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		float a = 0;
		float b = 0;
		float aux = 0;

		a = obtervalorA(scanner);

		b = obterValorB(scanner);

		aux = trocaDeValores(a);
		a = trocaDeValores(b);
		b = trocaDeValores(aux);

		exibirResultado(a, b);

	}

	private static float obtervalorA(Scanner scanner) {
		float a;
		System.out.println("Informe o valor de A: ");
		a = scanner.nextFloat();
		System.out.println("O valor digitado foi: " + a);
		return a;
	}

	private static float obterValorB(Scanner scanner) {
		float b;
		System.out.println("\nInforme o valor de B: ");
		b = scanner.nextFloat();
		System.out.println("O valor digitado foi: " + b);
		return b;
	}

	private static float trocaDeValores(float aux) {
		float b;
		b = aux;
		return b;
	}

	private static void exibirResultado(float a, float b) {
		System.out.println("\nO valor de A agora �: " + a);
		System.out.println("O valor de B agora �: " + b);
	}

}

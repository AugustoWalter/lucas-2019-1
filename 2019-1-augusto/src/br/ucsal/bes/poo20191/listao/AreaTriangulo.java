package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 5. Fa�a um algoritmo que calcule a �rea de um tri�ngulo, considerando a f�rmula �REA =
( BASE*ALTURA ) / 2. Utilize as vari�veis AREA, BASE e ALTURA e os operadores
aritm�ticos de multiplica��o e divis�o.
 */
public class AreaTriangulo {

	public static void main(String[] args) {
		float area = 0;
		float base = 0;
		float altura = 0;

		base = obterBase("Digite a base do tri�ngulo:", base);
		altura = obterAltura("Digite a altura do tri�ngulo", altura);
		area = calcularArea(area, base, altura);
		exibirArea("A �rea do tri�ngulo �: ", area);

	}

	private static float obterBase(String mensagem, float base) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return base = scanner.nextFloat();
	}

	private static float obterAltura(String mensagem, float altura) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return altura = scanner.nextFloat();
	}

	private static float calcularArea(float area, float base, float altura) {
		area = (base * altura) / 2;
		return area;
	}

	private static void exibirArea(String mensagem, float area) {
		System.out.println(mensagem + area);

	}

}

package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 14. Faça um algoritmo que leia quatro números e apresente os resultados de adição e
multiplicação dos valores entre si, baseando-se na utilização da propriedade distributiva,
ou seja, se forem lidas as variáveis A, B, C e D, devem ser somadas e multiplicadas A
com B, A com C e A com D; B com C, B com D e por último C com D.
 */

public class CalculoDistributivo {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		float a = 0;
		float b = 0;
		float c = 0;
		float d = 0;
		float soma1 = 0;
		float multiplicacao1 = 0;
		float soma2 = 0;
		float multiplicacao2 = 0;
		float soma3 = 0;
		float multiplicacao3 = 0;
		float soma4 = 0;
		float multiplicacao4 = 0;
		float soma5 = 0;
		float multiplicacao5 = 0;
		float soma6 = 0;
		float multiplicacao6 = 0;
	
		a = obterA(scanner);

		b = obterB(scanner);

		c = obterC(scanner);

		d = obterD(scanner);

		soma1 = somarAB(a, b);

		multiplicacao1 = multiplicarAB(a, b);

		soma2 = somarAC(a, c);

		multiplicacao2 = multiplicarAC(a, c);

		soma3 = somarAD(a, d);

		multiplicacao3 = multiplicarAD(a, d);

		soma4 = somarBC(b, c);

		multiplicacao4 = multiplicarBC(b, c);

		soma5 = somarBD(b, d);

		multiplicacao5 = multiplicarBD(b, d);

		soma6 = somarCD(c, d);

		multiplicacao6 = multiplicarCD(c, d);

		exibirResultado(soma1, multiplicacao1, soma2, multiplicacao2, soma3, multiplicacao3, soma4, multiplicacao4,
				soma5, multiplicacao5, soma6, multiplicacao6);

	}

	private static float obterA(Scanner scanner) {
		float a;
		System.out.println("Informe o valor A: ");
		a = scanner.nextFloat();
		return a;
	}

	private static float obterB(Scanner scanner) {
		float b;
		System.out.println("Informe o Valor B: ");
		b = scanner.nextFloat();
		return b;
	}

	private static float obterC(Scanner scanner) {
		float c;
		System.out.println("Informe o valor C: ");
		c = scanner.nextFloat();
		return c;
	}

	private static float obterD(Scanner scanner) {
		float d;
		System.out.println("Informe o valor D: ");
		d = scanner.nextFloat();
		return d;
	}

	private static float somarAB(float a, float b) {
		float soma1;
		soma1 = a + b;
		return soma1;
	}

	private static float multiplicarAB(float a, float b) {
		float multiplicacao1;
		multiplicacao1 = multiplicarAD(a, b);
		return multiplicacao1;
	}

	private static float somarAC(float a, float c) {
		float soma2;
		soma2 = somarAB(a, c);
		return soma2;
	}

	private static float multiplicarAC(float a, float c) {
		float multiplicacao2;
		multiplicacao2 = multiplicarAD(a, c);
		return multiplicacao2;
	}

	private static float somarAD(float a, float d) {
		float soma3;
		soma3 = somarAB(a, d);
		return soma3;
	}

	private static float multiplicarAD(float a, float d) {
		float multiplicacao3;
		multiplicacao3 = multiplicarCD(a, d);
		return multiplicacao3;
	}

	private static float somarBC(float b, float c) {
		float soma4;
		soma4 = somarAB(b, c);
		return soma4;
	}

	private static float multiplicarBC(float b, float c) {
		float multiplicacao4;
		multiplicacao4 = multiplicarAD(b, c);
		return multiplicacao4;
	}

	private static float somarBD(float b, float d) {
		float soma5;
		soma5 = somarAB(b, d);
		return soma5;
	}

	private static float multiplicarBD(float b, float d) {
		float multiplicacao5;
		multiplicacao5 = multiplicarAD(b, d);
		return multiplicacao5;
	}

	private static float somarCD(float c, float d) {
		float soma6;
		soma6 = somarAB(c, d);
		return soma6;
	}

	private static float multiplicarCD(float c, float d) {
		float multiplicacao6;
		multiplicacao6 = c * d;
		return multiplicacao6;
	}

	private static void exibirResultado(float soma1, float multiplicacao1, float soma2, float multiplicacao2,
			float soma3, float multiplicacao3, float soma4, float multiplicacao4, float soma5, float multiplicacao5,
			float soma6, float multiplicacao6) {
		System.out.println("A soma entre A e B: " + soma1);
		System.out.println("A multiplicação entre A e B: " + multiplicacao1);

		System.out.println("\nA soma entre A e C: " + soma2);
		System.out.println("\nA multiplicação entre A e C: " + multiplicacao2);

		System.out.println("\nA soma entre A e D: " + soma3);
		System.out.println("\nA multiplicação entre A e D: " + multiplicacao3);

		System.out.println("\nA soma entre B e C: " + soma4);
		System.out.println("\nA multiplicação entre B e C: " + multiplicacao4);

		System.out.println("\nA soma entre B e D: " + soma5);
		System.out.println("\nA multiplicação entre B e D: " + multiplicacao5);

		System.out.println("\nA soma entre C e D: " + soma6);
		System.out.println("\nA multiplicação entre C e D: " + multiplicacao6);
	}
}

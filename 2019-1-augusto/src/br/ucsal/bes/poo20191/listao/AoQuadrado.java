package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 17. Fa�a um algoritmo que leia dois valores inteiros (A e B) e apresente o resultado do
quadrado da soma dos valores lidos.
 */

public class AoQuadrado {

	public static void main(String[] args) {
		int valorA = 0;
		int valorB = 0;
		int total = 0;
		double quadrado = 0;

		valorA = obterValorA("Informe o valor A: ", valorA);
		valorB = obterValorB("Informe o valor B: ", valorB);

		total = calcularSoma("Soma de A + B: ", total, valorA, valorB);

		quadrado = calcularQuadrado(quadrado, total);

		exibirResultado("\nValor ao quadrado: ", quadrado);

	}

	private static int obterValorA(String mensagem, int valorA) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return valorA = scanner.nextInt();
	}

	private static int obterValorB(String mensagem, int valorB) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return valorB = scanner.nextInt();
	}

	private static int calcularSoma(String mensagem, int total, int valorA, int valorB) {
		total = valorA + valorB;
		System.out.println(mensagem + total);
		return total;
	}

	private static double calcularQuadrado(double quadrado, int total) {
		quadrado = total;
		quadrado = Math.pow(total, 2);
		return quadrado;
	}

	private static void exibirResultado(String mensagem, double quadrado) {
		System.out.println(mensagem + quadrado);

	}

}

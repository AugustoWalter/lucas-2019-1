package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 16. Fa�a um algoritmo que leia um valor inteiro e apresente os resultados do quadrado e
do cubo do valor lido.
 */

public class ValorQuadradoCubo {

	public static void main(String[] args) {
		double valor = 0;
		double quadrado = 0;
		double cubo = 0;

		valor = obterValor("Informe um valor", valor);

		quadrado = calcularQuadrado(quadrado, valor);

		cubo = calcularCubo(cubo, valor);

		exibirResultado("O valor ao quadrado �: ", quadrado, "\nO valor ao cubo �: ", cubo);

	}

	private static double obterValor(String mensagem, double valor) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return valor = scanner.nextDouble();
	}

	private static double calcularQuadrado(double quadrado, double valor) {
		quadrado = Math.pow(valor, 2);
		return quadrado;
	}

	private static double calcularCubo(double cubo, double valor) {
		cubo = Math.pow(valor, 3);
		return cubo;
	}

	private static void exibirResultado(String mensagem, double quadrado, String mensagem2, double cubo) {
		System.out.println(mensagem + quadrado + mensagem2 + cubo);

	}
}

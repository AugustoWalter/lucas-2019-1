package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 7. Fa�a um algoritmo que:
a) Leia um n�mero inteiro;
b) Leia um segundo n�mero inteiro;
c) Efetue a adi��o dos dois valores;
d) Apresente o valor calculado.
 */

public class Adicao {

	public static void main(String[] args) {
		int num1 = 0;
		int num2 = 0;
		int resultado = 0;
		num1 = obterNumero1("Digite o primeiro n�mero: ", num1);
		num2 = obterNumero2("Digite o segundo n�mero: ", num2);

		resultado = calcularAdicao(resultado, num1, num2);

		exibirResultado("O valor total da adi��o destes dois n�meros �: ", resultado);

	}

	private static int obterNumero1(String mensagem, int num1) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return num1 = scanner.nextInt();
	}

	private static int obterNumero2(String mensagem, int num2) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return num2 = scanner.nextInt();
	}

	private static int calcularAdicao(int resultado, int num1, int num2) {
		resultado = num1 + num2;
		return resultado;
	}

	private static void exibirResultado(String mensagem, int resultado) {
		System.out.println(mensagem + resultado);

	}

}

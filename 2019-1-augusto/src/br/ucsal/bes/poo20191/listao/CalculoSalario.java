package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 8. Fa�a um algoritmo que:
a) Obtenha o valor para a vari�vel HT (horas trabalhadas no m�s);
b) Obtenha o valor para a vari�vel VH (valor hora trabalhada):
c) Obtenha o valor para a vari�vel PD (percentual de desconto);
d) Calcule o sal�rio bruto => SB = HT * VH;
e) Calcule o total de desconto => TD = (PD/100) * SB;
f) Calcule o sal�rio l�quido => SL = SB � TD;
g) Apresente os valores de: Horas trabalhadas, Sal�rio Bruto, Desconto, Sal�rio
Liquido.
 */
public class CalculoSalario {

	public static void main(String[] args) {
		float ht = 0;
		float vh = 0;
		float pd = 0;
		float td = 0;
		float salarioBruto = 0;
		float salarioLiquido = 0;

		ht = obterHorasTrabalhadas("Informe Horas Trabalhadas: ", ht);
		vh = obterValorHoraTrabalhada("Informe Valor Hora R$: ", vh);
		pd = obterPercentualDeDesconto("Informe Percentual de Desconto %: ", pd);

		salarioBruto = calcularSalarioBruto(salarioBruto, ht, vh);

		td = calcularTodalDeDesconto(pd, td, salarioBruto);

		salarioLiquido = calcularSalarioLiquido(salarioBruto, td, salarioBruto);

		exibirHorasTrabalhadas("Quantidade de Horas Trabalhadas: ", ht);
		exirbirSalarioBruto("Valor de Sal�rio Bruto R$: ", salarioBruto);
		exibirDesconto("Valor de Desconto R$: ", td);
		exibirSalarioLiquido("Valor de Sal�rio L�quido R$: ", salarioLiquido);

	}

	private static float obterHorasTrabalhadas(String mensagem, float ht) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return ht = scanner.nextFloat();
	}

	private static float obterValorHoraTrabalhada(String mensagem, float vh) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return vh = scanner.nextFloat();
	}

	private static float obterPercentualDeDesconto(String mensagem, float pd) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return pd = scanner.nextFloat();
	}

	private static float calcularSalarioBruto(float salarioBruto, float ht, float vh) {
		salarioBruto = ht * vh;
		return salarioBruto;
	}

	private static float calcularTodalDeDesconto(float pd, float td, float salarioBruto) {
		td = (pd / 100) * salarioBruto;
		return td;
	}

	private static float calcularSalarioLiquido(float salarioLiquido, float td, float salarioBruto) {
		salarioLiquido = salarioBruto - td;
		return salarioLiquido;
	}

	private static void exibirHorasTrabalhadas(String mensagem, float ht) {
		System.out.println(mensagem + ht);

	}

	private static void exirbirSalarioBruto(String mensagem, float salarioBruto) {
		System.out.println(mensagem + salarioBruto);

	}

	private static void exibirDesconto(String mensagem, float td) {
		System.out.println(mensagem + td);

	}

	private static void exibirSalarioLiquido(String mensagem, float salarioLiquido) {
		System.out.println(mensagem + salarioLiquido);
	}

}

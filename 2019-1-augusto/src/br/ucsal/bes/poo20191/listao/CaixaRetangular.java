package br.ucsal.bes.poo20191.listao;

import java.util.Scanner;

/*
 * 15. Fa�a um algoritmo que leia os valores de COMPRIMENTO, LARGURA e ALTURA e
apresente o valor do volume de uma caixa retangular. Utilize para o c�lculo a f�rmula:
VOLUME = COMPRIMENTO * LARGURA * ALTURA
 */

public class CaixaRetangular {

	public static void main(String[] args) {
		float comprimento = 0;
		float largura = 0;
		float altura = 0;
		float volume = 0;

		comprimento = obterComprimento("Informe o comprimento da Caixa Retangular: ", comprimento);
		largura = obterLargura("Informe a largura da Caixa Retangular: ", largura);
		altura = obterAltura("Informe a altura da Caixa Retangular: ", altura);

		volume = calcularVolume(volume, comprimento, largura, altura);

		exibirVolume("O valor do Volume da Caixa Retangular �: ", volume);

	}

	private static float obterComprimento(String mensagem, float comprimento) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return comprimento = scanner.nextFloat();
	}

	private static float obterLargura(String mensagem, float largura) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return largura = scanner.nextFloat();
	}

	private static float obterAltura(String mensagem, float altura) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(mensagem);
		return altura = scanner.nextFloat();
	}

	private static float calcularVolume(float volume, float comprimento, float largura, float altura) {
		volume = comprimento * largura * altura;
		return volume;
	}

	private static void exibirVolume(String mensagem, float volume) {
		System.out.println(mensagem + volume);

	}

}

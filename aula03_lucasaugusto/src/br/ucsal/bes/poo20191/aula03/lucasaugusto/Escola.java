package br.ucsal.bes.poo20191.aula03.lucasaugusto;

import java.time.LocalDate;

public class Escola {

	public static void main(String[] args) {
		Professor professor1 = new Professor();
		Professor professor2 = new Professor();

		professor1.nome = "Lucas";
		professor1.email = "lucas.walter@ucsal.edu.br";
		professor1.matricula = 200001172;
		professor1.dataNascimento = LocalDate.of(1994, 11, 3);
		professor1.dataContratação = LocalDate.of(2019, 03, 6);

		professor2.nome = "Dudu";
		professor2.email = "lucasassuncao2014@gmail.com";
		professor2.matricula = 200001173;
		professor2.dataNascimento = LocalDate.of(1996, 12, 8);
		professor2.dataContratação = LocalDate.of(2018, 11, 5);

		// System.out.println("professor1=" + professor1);
		// System.out.println("professor2=" + professor2);

		System.out.println("professor1.nome = " + professor1.nome);
		System.out.println("professor2.nome = " + professor2.nome);
		System.out.println("Professor1.email = " + professor1.email);
		System.out.println("Professor2.email = " + professor2.email);
		System.out.println("Professor1.dataNascimento = " + professor1.dataNascimento);
		System.out.println("Professor2.dataNascimento = " + professor2.dataNascimento);
		System.out.println("Professor1.dataContratação = " + professor1.dataContratação);
		System.out.println("Professor2.dataContratação = " + professor2.dataContratação);

	}
}

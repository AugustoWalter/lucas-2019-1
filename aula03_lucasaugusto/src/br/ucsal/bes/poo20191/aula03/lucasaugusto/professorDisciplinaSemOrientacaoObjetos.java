package br.ucsal.bes.poo20191.aula03.lucasaugusto;

import java.util.Date;

public class professorDisciplinaSemOrientacaoObjetos {

	private static final int QTD_PROFESSOR = 10;
	private static final int QTD_DISCIPLINA = 5;

	public static void main(String[] args) {

		String nomesProfessores[] = new String[QTD_PROFESSOR];
		Integer matriculasProfessores[] = new Integer[QTD_DISCIPLINA];
		String emailsProfessores[] = new String[QTD_PROFESSOR];
		Date dataNascimentoProfessores[] = new Date[QTD_PROFESSOR];
		Date datasContratacaoProfessor[] = new Date[QTD_PROFESSOR];

		String codigosDisciplinas[] = new String[QTD_DISCIPLINA];
		String nomesDisciplinas[] = new String[QTD_DISCIPLINA];
		Integer cargasHorariasDisciplinas[] = new Integer[QTD_DISCIPLINA];

		// cria��o do m�todo
		cadastrarProfessores(nomesProfessores, matriculasProfessores, emailsProfessores, dataNascimentoProfessores,
				datasContratacaoProfessor);

		cadastrarDisciplinas(codigosDisciplinas, nomesDisciplinas, cargasHorariasDisciplinas);

	} // exemplo sem oritenta��o a objetos

	private static void cadastrarDisciplinas(String[] codigosDisciplinas, String[] nomesDisciplinas,
			Integer[] cargasHorariasDisciplinas) {

	}

	private static void cadastrarProfessores(String[] nomesProfessores, Integer[] matriculasProfessores,
			String[] emailsProfessores, Date[] dataNascimentoProfessores, Date[] datasContratacaoProfessor) {

	}

}

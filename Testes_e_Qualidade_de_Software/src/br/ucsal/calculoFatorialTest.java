package br.ucsal;

import org.junit.Test;

import junit.framework.Assert;

public class calculoFatorialTest {

	@Test
	public void testarFatorial5() {
		// Setup

		// Definir dados de entrada
		Integer n = 5;

		// Definir a sa�da esperada
		Long fatorialEsperado = 120L;

		// Executar o m�todo que ser� testado e obter o resultado atual.
		Long fatorialAtual = calculoFatorial.calcularFatorial(n);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);

	}
}

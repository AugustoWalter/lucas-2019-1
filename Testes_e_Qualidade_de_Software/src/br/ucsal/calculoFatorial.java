package br.ucsal;

import java.util.Scanner;

public class calculoFatorial {

	public static void main(String[] args) {
		Integer n;
		Long fatorial;

		n = obterNumero();
		fatorial = calcularFatorial(n);
		exibirFatorial(n, fatorial);
	}

	public static Integer obterNumero() {
		Scanner scanner = new Scanner(System.in);
		Integer n;
		while (true) {
			System.out.println("Informe um nmero (0 a 100, intervalo fechado):");
			n = scanner.nextInt();
			if (n >= 0 && n <= 100) {
				return n;
			} else {
				System.out.println("Nmero fora da faixa!");
			}
		}
	}

	public static Long calcularFatorial(Integer n) {
		Long fatorial = 1L;
		for (Integer i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

	public static void exibirFatorial(Integer n, Long fatorial) {
		System.out.println("Fatorial(" + n + ")=" + fatorial);
	}

}

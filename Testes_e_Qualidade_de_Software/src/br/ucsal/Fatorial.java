package br.ucsal;

import java.util.Scanner;

/*
 *   Obtenha um n�mero entre 0 e 100.
 *   Realize o c�lculo do fatorial do n�mero informado.
 *   Exiba o fatorial calculado.
 */

public class Fatorial {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int res = 0;
		double fat = 1;

		do {
			int num = obterNumero(scanner);
			res = num;
			fat = calcularFatorial(fat, num);

		} while (res < 0 || res > 100);

		exibirResultado(res, fat);
	}

	public static int obterNumero(Scanner scanner) {
		System.out.println("Digite um n�mero inteiro e positivo, entre 0 e 100: ");
		int num = scanner.nextInt();
		return num;
	}

	public static double calcularFatorial(double fat, int num) {
		if (num >= 0 && num <= 100) {

			for (int i = 1; i <= num; i++) {
				fat *= i;
			}

		} else {
			System.out.println("ERRO!");
		}
		return fat;
	}

	public static void exibirResultado(int res, double fat) {
		System.out.println("O n�mero digatado foi: " + res + " e seu fatorial �" + " = " + fat);
	}
}
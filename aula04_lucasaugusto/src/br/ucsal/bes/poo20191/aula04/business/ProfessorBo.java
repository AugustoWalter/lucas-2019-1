package br.ucsal.bes.poo20191.aula04.business;

import br.ucsal.bes.poo20191.aula04.domain.Professor;
import br.ucsal.bes.poo20191.aula04.persistence.ProfessorDAO;

public class ProfessorBo {
//negocios, vida do sistema
	public static String cadastrar(Professor professor) {
		if (professor == null) {
			// FIXME mudar para utilizar exception!
			return "Erro: professor n�o informado.";
		}

		if (professor.getNome() != null && professor.getNome().trim().isEmpty()) {
			// FIXME mudar para utilizar exception!
			return "Erro: professor n�o informado.";
		}
		if (professor.getMatricula() == null) {
			// FIXME mudar para utilizar exception!
			return "Erro: professor n�o informado.";

		}
		ProfessorDAO.inserir(professor);
		return "Sucesso, professsor cadastrado.";

	}

	public static Professor consultar(Integer matricula) {
		return ProfessorDAO.consultar(matricula);
	}
}

package br.ucsal.bes.poo20191.aula04.domain;

import java.time.LocalDate;

public class Professor {

	private String nome;

	private Integer matricula;

	private String email;

	private LocalDate dataNascimento;

	private LocalDate dataContratação;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public LocalDate getDataContratação() {
		return dataContratação;
	}

}
package br.ucsal.bes.poo20191.aula04.tui;

import java.util.Scanner;

import br.ucsal.bes.poo20191.aula04.business.ProfessorBo;
import br.ucsal.bes.poo20191.aula04.domain.Professor;

public class ProfessorTui {
//interface console
	public static void main(String[] args) {
		Professor professor;
		String mensagem;

		professor = ProfessorTui.obterDados();
		mensagem = ProfessorBo.cadastrar(professor);
		System.out.println("Mensagem: " + mensagem);

		professor = ProfessorTui.obterDados();
		ProfessorBo.cadastrar(professor);
		System.out.println("Mensagem: " + mensagem);

		consultar();

	}

	private static void consultar() {
		Scanner scanner = new Scanner(System.in);
		Integer matricula;
		Professor professor;
		while (true) {
			System.out.println("Informe a matr�cula: ");
			matricula = scanner.nextInt();
			professor = ProfessorBo.consultar(matricula);
			if (professor == null) {
				System.out.println("Professor n�o encontrado.");

			} else {
				System.out.println("Nome: " + professor.getNome());
			}
		}

	}

	static Professor obterDados() { // ou passar pra Static
		Scanner scanner = new Scanner(System.in);
		String nome;
		Integer matricula;
		String email;

		System.out.println("Informe o nome: ");
		nome = scanner.nextLine();
		System.out.println("Informe a matr�cula: ");
		matricula = scanner.nextInt();
		scanner.nextLine(); // gambiarra necessaria
		System.out.println("Informe email: ");
		email = scanner.nextLine();

		System.out.println("nome: " + nome);
		System.out.println("matr�cula: " + matricula);
		System.out.println("email: " + email);

		Professor professor = new Professor();
		professor.setNome(nome);
		professor.setMatricula(matricula);
		professor.setEmail(email);

		return professor;

	}

}